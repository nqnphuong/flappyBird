import { Container, TextStyle, Text} from "pixi.js";
import { GAME_HEIGHT, GAME_WIDTH } from "../constant";
import { getSpriteFromCache } from "../utils/utils";

export class LoseScene extends Container {
    constructor() {
        super();
        this.loseSceneContainer = new Container();
        this.loseScene();
    }

    loseScene() {
        const style = new TextStyle({
            fontFamily: "Franklin Gothic Heavy",
            fontSize: 64,
            fill: "white"
        });
        this.message = new Text("YOU LOSE", style);
        this.message.x = GAME_WIDTH/2 - (this.message.width/2);
        this.message.y = GAME_HEIGHT / 2 - 120;
        this.loseSceneContainer.addChild(this.message);
        this.playButton();
    }

    playButton(){
        this.playBtn = getSpriteFromCache("playButton.png");
        this.playBtn.x = GAME_WIDTH/2 - (this.playBtn.width/2);
        this.playBtn.y = GAME_HEIGHT / 2 -20;
        this.loseSceneContainer.addChild(this.playBtn);
    }

}