import { Collider } from "./collider";

export class ColliderBomb extends Collider{
    checkCollider(objectToCheck){
        if (objectToCheck.type === PIPES_COLLIDER) {
            return this.detectPipes(this.obj1, objectToCheck);
        }
    }
    
    detectPipes(r1, r2) {
        if (r1.x + r1.width > r2.x && r1.y + r1.height < r2.y && r1.y > r2.y + r2.height) {
            return true;
        }
        return false;
    }
}
