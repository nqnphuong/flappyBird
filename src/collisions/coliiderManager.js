import { utils } from "pixi.js";

export const Ignore = [
    ["Pipes", "Laze"],
    ["Bomb", "Laze"]
]
export class ColliderManager extends utils.EventEmitter {
    constructor() {
        super();
        // this.colliderList = ["Bird", "Pipes", "Bomb", "Laze"];   
        this.colliderList = [];
    }

    addCollider(collider){
        let colliderObject = {
            type: collider.constructor.name,
            collider: collider
        }
        this.colliderList.push(colliderObject);
    }

    checkCollider() {
        for (const i = 0; i < this.colliderList.length - 2; i++) {
            for (const j = i + 1; j < this.colliderList.length - 1; j++) {
                if (!Ignore.includes([this.colliderList[i], this.colliderList[j]]) && !Ignore.includes([this.colliderList[j], this.colliderList[i]])) {
                    col1 = this.colliderList[i];
                    col2 = this.colliderList[j];
                    console.log(col1, col2);
                    if (col1.checkCollider(col2)) {
                        this.emit("collide", col1, col2);
                        col1.emit("collide", col2);
                        col2.emit("collide", col1);
                    }
                }
            }
        }
    }
}