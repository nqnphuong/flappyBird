import { utils } from "pixi.js";

export class Collider extends utils.EventEmitter{
    constructor(type, obj1) {
        super();
        this.type = type;
        this.obj1 = obj1;
    }

    checkCollider(objectToCheck) {
        throw new Error("Must override collision");
    }


    ////////////////////////////////////////////////////////////////////////////
    detectCollisions(r1, r2) { //oke
        if (r2.note == "top") {
            if ((r1.y < r2.y) && (r1.x + r1.width > r2.x) && (r1.x < r2.x + r2.width)) {
                return true;
            }
        }
        if (r2.note == "bot") {
            if ((r1.y + r1.height > r2.y) && (r1.x + r1.width > r2.x) && (r1.x < r2.x + r2.width)) {
                return true;
            }
        }
        return false;
    }

    detectLaze(r1, r2) { //oke
        if (r1.y < r2.posY && r1.y + r1.height > r2.posY) {
            return true;
        }
        return false;
    }

    detectBomb(r1, r2) { //oke
        if (r1.x + r1.width >= r2.x && r1.x <= r2.x + r2.width) {
            if (r1.y >= r2.y && r1.y <= r2.y + r2.height){
                return true;
            } else if (r1.y + r1.height <= r2.y && r1.y + r1.height >= r2.y + r2.height){
                return true;
            }
        }
        return false;
    }

    detectPipes(r1, r2) {
        if (r1.x + r1.width > r2.x && r1.y + r1.height < r2.y && r1.y > r2.y + r2.height) {
            return true;
        }
        return false;
    }

















    detectCollisions(r1, r2) {
        let hit;
        hit = false;

        if (r2.note == "top") {
            if ((r1.y < r2.y) && (r1.x + r1.width > r2.x) && (r1.x < r2.x + r2.width)) {
                hit = true;
            }
        }
        if (r2.note == "bot") {
            if ((r1.y + r1.height > r2.y) && (r1.x + r1.width > r2.x) && (r1.x < r2.x + r2.width)) {
                hit = true;
            }
        }
        return hit;
    }
}