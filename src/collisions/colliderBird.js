import { PIPES_COLLIDER } from "../constant";
import { Collider } from "./collider";

export class CollderBird extends Collider{
    
    checkCollider(objectToCheck) {
        if (objectToCheck.type === PIPES_COLLIDER) {
            return this.detectPipes(this.obj1, objectToCheck);
        } else if (objectToCheck.type === LAZE_COLLIDER) {
            return this.detectLaze(this.obj1, objectToCheck);
        } else if (objectToCheck.type === BOMB_COLLIDER) {
            return this.detectBomb(this.obj1, objectToCheck);
        }
    }

    detectPipes(r1, r2) {
        if (r2.note == "top") {
            if ((r1.y < r2.y) && (r1.x + r1.width > r2.x) && (r1.x < r2.x + r2.width)) {
                return true;
            }
        }
        if (r2.note == "bot") {
            if ((r1.y + r1.height > r2.y) && (r1.x + r1.width > r2.x) && (r1.x < r2.x + r2.width)) {
                return true;
            }
        }
        return false;
    }

    detectLaze(r1, r2) {
        if (r1.y < r2.y && r1.y + r1.height > r2.y) {
            return true;
        }
        return false;
    }

    detectBomb(r1, r2) {
        if (r1.x + r1.width < r2.x) {
            if (r1.y > r2.y + r2.height || r1.y + r1.height < r2.y) {
                return false
            } else {
                return true;
            }
        }
        return false;
    }
}