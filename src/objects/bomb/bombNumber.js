import { Container, TextStyle, Text } from "pixi.js";
import { GAME_WIDTH } from "../../constant";

export class BombNumber extends Container {
    constructor() {
        super();
        this.bombNumber = 0;
    }

    createBombNumber() {
        const style = new TextStyle({
            fontFamily: "Franklin Gothic Heavy",
            fontSize: 20,
            fill: "white",
            style: "bold"
        });
        this.messageBomb = new Text("Bomb: " + this.bombNumber, style);
        this.messageBomb.x = GAME_WIDTH - 100;
        this.messageBomb.y = 60;
        this.addChild(this.messageBomb);

    }

    updateBombNumber() {
        this.bombNumber += 1;
        this.messageBomb.text = "Bomb: " + this.bombNumber;
    }
}