import { Container } from "pixi.js";
import { SPEED_OBJECT, GAME_WIDTH } from "../../constant";
import { randomInt, getSpriteFromCache } from "../../utils/utils";

export class Bomb extends Container {
    constructor() {
        super();
    }

    createBomb() {
        this.bomb = getSpriteFromCache("bomb.png");
        const y = randomInt();
        this.bomb.position.set(GAME_WIDTH + 500, y - 100);
        this.addChild(this.bomb);
    }

    updateBomb() {
        if (this.bomb) {
            this.bomb.x -= SPEED_OBJECT;
        }

    }

    removeBomb(bomb){
        this.removeChild(bomb);
    }
}