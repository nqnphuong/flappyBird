import { Container, AnimatedSprite} from "pixi.js";
import { getSpriteFromCache, getTextureFromCache } from "../utils/utils";
import { V_MAX, V_MIN, FORCE , G, KEYBOARD_PRESS, GAME_HEIGHT} from "../constant";
import { KBEvent, Keyboard } from "../input/keyboard";
import { sound } from "@pixi/sound";
export class Bird extends Container {
    constructor() {
        super();
        this.v = 0;
        this.fly();
    }

    createBird() {
        this.bird = getSpriteFromCache("bird1.png");
        this.animationFly();
        this.bird.position.set(160, 500);
        this.addChild(this.bird);
    }

    // cho animation sau khi con chim bay -> cần fix lại
    animationFly() {
        let birdFrames = [
            "bird1.png",
            "bird2.png",
            "bird3.png",
            "bird4.png",
            "bird3.png",
            "bird2.png",
        ];
        let birdObject = [];
        for (let i = 0; i < 6; i++) {
            birdObject.push(getTextureFromCache(birdFrames[i]));
        }
        this.bird = new AnimatedSprite(birdObject);
        this.bird.animationSpeed = 0.2;
        this.bird.play();
    }

    stopAnimation(){
        this.bird.stop();
    }

    fly(){
        Keyboard.instance.on(KBEvent.KeyDown, e => { // on la dung nhieu lan
            if (e.code === KEYBOARD_PRESS){
                if (this.v + FORCE > V_MAX){
                    this.v += FORCE;
                    this.bird.y -= this.v;
                    sound.play('flySound');
                }
            }
        });
    }

    update() {
        // this.animationFly();
        // console.log(this.v);
        this.v += G;
        this.bird.y += this.v;
    }

    fallToGround(){
        if (this.bird.y + this.bird.height >= GAME_HEIGHT){
            return true;
        }
        return false;
    }
}