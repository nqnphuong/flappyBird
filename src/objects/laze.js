import { Container, Graphics } from "pixi.js";
import { getSpriteFromCache, randomInt } from "../utils/utils";
import { GAME_WIDTH } from "../constant";
import { sound } from "@pixi/sound";

export class Laze extends Container {
    constructor() {
        super();
        this.isEnable = true;
    }

    createLaze() {
        this.warning = getSpriteFromCache("warning.png");
        const y = randomInt();
        this.warning.position.set(GAME_WIDTH - 5 - this.warning.width, y);
        this.addChild(this.warning);

        this.warningFlicker = setInterval(() => {
            this.warning.alpha = (this.warning.alpha === 1) ? 0.5 : 1;
        }, 100);

        setTimeout(() => {
            this.line = new Graphics();
            this.line.lineStyle(2, 0xff0000); // red
            this.line.moveTo(0, y + this.warning.height / 2);
            this.line.lineTo(GAME_WIDTH, y + this.warning.height / 2);
            this.addChild(this.line);
            sound.play('lazeSound')
            this.line.posY = y + this.warning.height / 2;

            this.removeChild(this.warning);
            setTimeout(() => {
                clearInterval(this.warningFlicker);
                this.isEnable = false;
                this.removeChild(this.line);
            }, 200);
        }, 2000);
    }
}