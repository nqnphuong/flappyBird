import { Application, Assets } from "pixi.js";
import { GAME_HEIGHT, GAME_WIDTH, KEYBOARD_PRESS } from "./constant";
import { PlayScene } from "./scenes/playScene";
import { LoseScene } from "./scenes/loseScene";
import { Collider } from "./collisions/collider";
import { BeginScene } from "./scenes/beginScene";
import { KBEvent, Keyboard } from "./input/keyboard";
import { MEvent, Mouse } from "./input/mouse";
import { ColliderManager } from "./collisions/coliiderManager";
import { sound } from "@pixi/sound";
export default class Game {
    constructor() {
        this.app = new Application({
            width: GAME_WIDTH,
            height: GAME_HEIGHT,
            antialias: true,
            transparent: false
        });
        document.body.appendChild(this.app.view);
        Keyboard.init();
        Mouse.init();
    }

    async loadAssets() {
        await Assets.load("./images/flappyBird.json");
        sound.add('scoreSound', '../audios/score.wav');
        sound.add('flySound', '../audios/fly.wav');
        sound.add('dieSound', '../audios/crash.wav');
        sound.add('lazeSound', '../audios/laze.mp3');
        sound.add('buttonSound', '../audios/clickButton.mp3');
        this.setup();
    }

    setup() {
        this.playScene = new PlayScene();
        this.app.stage.addChild(this.playScene.playSceneContainer);
        this.playScene.playSceneContainer.skipChildrenUpdate = true;

        this.loseScene = new LoseScene();
        this.app.stage.addChild(this.loseScene.loseSceneContainer);
        this.loseScene.loseSceneContainer.visible = false;

        this.beginScene = new BeginScene();
        this.app.stage.addChild(this.beginScene.beginSceneContainer);

        this.waitStart();
    }

    waitStart() {
        Keyboard.instance.once(KBEvent.KeyDown, e => { //once: dung 1 lan
            if (e.code === KEYBOARD_PRESS) {
                this.playScene.bird.fly();
                this.start();
            }
        })
    }

    start() {
        this.lazeActivity();
        // this.bombActivity();
        this.beginScene.beginSceneContainer.visible = false;
        this.playScene.playSceneContainer.skipChildrenUpdate = false;
        this.state = this.play;
        this.app.ticker.add((delta) => this.gameLoop(delta));
    }

    gameLoop(delta) {
        this.state(delta);
    }

    play() {
        this.playScene.updateBackground();
        this.playScene.pipes.update();
        this.playScene.bird.update();
        this.playScene.updateScore();
        // this.playScene.bomb.updateBomb();

        // collision
        let hit = false;
        this.collider = new Collider();
        this.playScene.pipes.pipesGroup.forEach(pipes => {
            pipes.forEach(pipe => {
                if (this.collider.detectCollisions(this.playScene.bird.bird, pipe)) {
                    hit = true;
                }
            });
        });

        if (this.playScene.laze !== undefined && this.playScene.laze.line !== undefined && this.playScene.laze.isEnable) {
            console.log("Laze tồn tại");
            if (this.collider.detectLaze(this.playScene.bird.bird, this.playScene.laze.line)) {
                hit = true;
            }
        }
        // if (this.playScene.bomb.bomb !== undefined) {
        //     if (this.collider.detectBomb(this.playScene.bird.bird, this.playScene.bomb.bomb)) {
        //         this.playScene.bomb.removeBomb(this.playScene.bomb.bomb);
        //         this.playScene.bombNumber.updateBombNumber();
        //     }
        // }


        // tu tu test sau
        // if (this.playScene.bombNumber.bombNumber > 0) {
            // this.playScene.pipesGroup.forEach(pipes => {
            //     pipes.forEach(pipe => {
            //         if (this.collider.detectCollisions(this.playScene.bomb.bomb, pipe)) {
            //             // hit = true;
            //             this.playScene.pipes.removeChild(pipe);
            //         }
            //     });
            // });
        // }



        if (this.playScene.bird.fallToGround()) {
            hit = true;
        }

        this.colliderMng = new ColliderManager();

        if (hit === true) {
            sound.play('dieSound');
            this.state = this.end;
        }
    }

    lazeActivity() {
        this.lazeInterval = setInterval(() => {
            this.playScene.createLaze();
        }, 6000);
    }

    // bombActivity() {
    //     this.bombInterval = setInterval(() => {
    //         this.playScene.createBomb();
    //     }, 12000);
    // }


    end() {
        clearInterval(this.lazeInterval);
        clearInterval(this.bombInterval);
        this.playScene.playSceneContainer.skipChildrenUpdate = false;
        this.playScene.bird.stopAnimation();
        this.loseScene.loseSceneContainer.visible = true;
        this.loseScene.playBtn.interactive = true;
        this.restart();
    }

    restart() {
        Mouse.instance.removeAllListeners(MEvent.MClick); // click only 1
        Mouse.instance.once(MEvent.MClick, () => {
            console.log("button press");
            sound.play('buttonSound');
            setTimeout(() => {  // den buoc duong cung :)) (fix sau)
                location.reload();
            }, 400);
            // this.loseScene.loseSceneContainer.visible = false;
        });
        // this.playScene.score.score = 0;
        // this.playScene.bombNumber.bombNumber = 0;
        // this.loseScene.loseSceneContainer.visible = false;
        // this.playScene.playSceneContainer.visible = true;
        // this.playScene.playSceneContainer.skipChildrenUpdate = true;
        // this.beginScene.beginSceneContainer.visible = true;

        // this.waitStart();
    }
}
